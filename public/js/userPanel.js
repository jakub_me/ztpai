document.querySelector("#changePassword").addEventListener("click", changePassword);
document.querySelector("#changeDetails").addEventListener("click", changeDetails);



function changePassword() {
    const data = {
        "data": {
            "oldPassword": document.querySelector("#oldPassword").value,
            "newPassword": document.querySelector("#newPassword").value
        }
    }
    if(data.oldPassword == "" || data.newPassword == ""){
        alert("Pola muszą być wypełnione!");
        return false;
    }
    fetch(`account/change-password`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
    }).catch(function (e){
        console.log(e);
    });
}

function changeDetails() {
    const data = {
        "data": {
            "name": document.querySelector("#name").value,
            "surname": document.querySelector("#surname").value
        }
    }
    fetch(`account/change-user-details`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
    }).catch(function (e){
        console.log(e);
    });
}
