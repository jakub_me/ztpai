const itemsContainer = document.querySelector("#container");

document.querySelectorAll(".add-item").forEach(bt => bt.addEventListener("click", addItem));
document.querySelectorAll('.save').forEach(bt => bt.addEventListener('click',updateItem));
document.querySelectorAll('.delete').forEach(bt => bt.addEventListener('click',deleteItem));
document.querySelectorAll('.savecookies').forEach(bt => bt.addEventListener('click',setAdminReference));
document.querySelectorAll('.searchInBase').forEach(bt => bt.addEventListener('click',searchInBase));

//category info
const categoryName = document.querySelector('#categoryName');
const categoryId = document.querySelector('#categoryId');

//disable admin reference if null cookies
if(localStorage.getItem('adminItemId') === null)
    document.querySelectorAll('.savecookies').forEach(bt => bt.disabled = true);


function fetchTemplate(type,id="", body=null){
    if(body != null) body = JSON.stringify(body);
    return fetch(`/user/items/${id}`, {
        method: type,
        headers: {
            'Content-Type': 'application/json',
        },
        body: body
    })
}

function fillData(record){
    const val = record.querySelector('input[name=adminItem]').value
    return {
        "data": {
            "categoryId": categoryId.value,
            "valuePerOne": parseFloat(record.querySelector('input[name=valuePerOne]').value)*100,
            "amount": record.querySelector('input[name=amount]').value,
            "adminItemId": val != "" ? val : null,
            "autoupdatable": record.querySelector('input[name=autoupdatable]').checked,
            "name": record.querySelector('input[name=name]').value,
        }
    }
}

function addItem() {
    const record = this.parentNode.parentNode;
    fetchTemplate("POST",  "", fillData(record))
    .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
        return response.json();
    }).then(function (item) {
        record.querySelector('input[name=valuePerOne]').value = "";
        record.querySelector('input[name=amount]').value = "";
        record.querySelector('input[name=adminItem]').value = "";
        record.querySelector('input[name=autoupdatable]').checked = false;
        record.querySelector('input[name=name]').value = "";
        displayItem(item.data);
    }).catch(function (e){
        console.log(e);
    });
}

function updateItem() {
    const record = this.parentNode.parentNode;
    fetchTemplate("PUT", this.value, fillData(record))
    .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
    }).catch(function (e){
        console.log(e);
    });
}

function deleteItem() {
    const record = this.parentNode.parentNode;
    fetchTemplate("DELETE", this.value, null)
    .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
    }).then(function () {
        record.parentNode.removeChild(record);
    }).catch(function (e){
        console.log(e);
    });
}


function setAdminReference(){
    const aid = localStorage.getItem('adminItemId');
    const aname = localStorage.getItem('adminItemName');
    if(aid === null) return;
    const record = this.parentNode.parentNode;
    record.querySelector('input[name=adminItem]').value=aid;
    const divs = record.querySelectorAll('.adminItemName');
    divs[0].innerHTML = aname;
    divs[1].innerHTML = "Przypisano z bazy: " + aname;
}

function searchInBase(){
    const record = this.parentNode.parentNode;
    localStorage.setItem('catName', categoryName.getAttribute('data-name'));
    localStorage.setItem('catId', categoryId.value);
    localStorage.setItem('itemName', record.querySelector('input[name=name]').value);
    localStorage.setItem('itemId', record.getAttribute('data-id'));
    //save item to edit + redirect
    window.location.href = `/searchbase`;
}


function displayItem(item) {
    const template = document.querySelector("#item-template");
    const clone = template.content.cloneNode(true);
    clone.querySelector('.record').setAttribute('data-id',item.id);
    clone.querySelector('input[name=name]').value = item.name;
    clone.querySelector('input[name=valuePerOne]').value = (parseFloat(item.valuePerOne)*0.01).toFixed(2);
    clone.querySelector('input[name=amount]').value = item.amount;
    if(item.autoupdatable) clone.querySelector('input[name=autoupdatable]').checked = true;

    const btsave = clone.querySelectorAll('.save');
    btsave[0].value = btsave[1].value = item.id;
    btsave[0].addEventListener('click',updateItem);
    btsave[1].addEventListener('click',updateItem);

    const btdelete = clone.querySelectorAll('.delete');
    btdelete[0].value = btdelete[1].value = item.id;
    btdelete[0].addEventListener('click',deleteItem);
    btdelete[1].addEventListener('click',deleteItem);

    const btsavecookies = clone.querySelectorAll('.savecookies');
    btsavecookies[0].value = btsavecookies[1].value = item.id;
    btsavecookies[0].addEventListener('click',setAdminReference);
    btsavecookies[1].addEventListener('click',setAdminReference);

    if(localStorage.getItem('adminItemId') === null){
        btsavecookies[0].disabled = true;
        btsavecookies[1].disabled = true;
    }

    const btsearchInBase = clone.querySelectorAll('.searchInBase');
    btsearchInBase[0].value = btsearchInBase[1].value = item.id;
    btsearchInBase[0].addEventListener('click',searchInBase);
    btsearchInBase[1].addEventListener('click',searchInBase);

    if ('adminItemId' in item){
        clone.querySelector('input[name=adminItem]').value=item.adminItemId;
        clone.querySelectorAll('.adminItemName').forEach(bt => bt.innerHTML += item.adminItemName);
    }
    itemsContainer.appendChild(clone);
}
