const itemsContainer = document.querySelector("#itemsContainer");
const categoryId = document.querySelector('input[type=hidden]');
const renameInput = document.querySelector("#category-name");

document.querySelector("#add").addEventListener("click", addItem);
document.querySelectorAll(".delete").forEach(btn => btn.addEventListener("click", deleteItem));
document.querySelectorAll(".save").forEach(btn => btn.addEventListener("click", updateItem));




function fetchTemplate(type, id="", body=null){
    if(body != null) body = JSON.stringify(body);
    return fetch(`/admin/userdb/${id}`, {
        method: type,
        headers: {
            'Content-Type': 'application/json',
        },
        body: body
    })
}

function addItem() {
    const record = document.querySelector('#new');
    let data = { data: {
        user: {
            email: record.querySelector('input[name=email]').value,
            plainPassword: record.querySelector('input[name=pass]').value,
        },
        userDetails: {
            name: record.querySelector('input[name=name]').value,
            surname: record.querySelector('input[name=surname]').value,
        },
        userRoles: {userTypeText: null }
    }};
    for(let i of record.querySelectorAll('input[type=radio]'))
        if(i.checked) data.data.userRoles.userTypeText = i.value;
    fetchTemplate("POST",  "", data)
        .then(function (response) {
            if(!response.ok)
                throw Error(response.status.toString());
            return response.json();
        }).then(function (item) {
        record.querySelector('input[name=email]').value = "";
        record.querySelector('input[name=pass]').value = "";
        record.querySelector('input[name=name]').value = "";
        record.querySelector('input[name=surname]').value = "";
        displayItem(item.data);
    }).catch(function (e){
        console.log(e);
    });
}

function updateItem() {
    const record = this.parentNode.parentNode;
    let data = { data: {
            user: {
                email: record.querySelector('input[name=email]').value,
            },
            userDetails: {
                name: record.querySelector('input[name=name]').value,
                surname: record.querySelector('input[name=surname]').value,
            },
            userRoles: {userTypeText: null }
        }};
    for(let i of record.querySelectorAll('input[type=radio]'))
        if(i.checked) data.data.userRoles.userTypeText = i.value;
    fetchTemplate("PUT", this.value, data)
        .then(function (response) {
            if(!response.ok)
                throw Error(response.status.toString());
        }).then(function () {
    }).catch(function (e){
        console.log(e);
    });
}

function deleteItem() {
    const record = this.parentNode.parentNode;
    fetchTemplate("DELETE", this.value, null)
        .then(function (response) {
            if(!response.ok)
                throw Error(response.status.toString());
        }).then(function () {
        record.parentNode.removeChild(record);
    }).catch(function (e){
        console.log(e);
    });
}

function displayItem(item) {
    const template = document.querySelector("#items-template");
    const clone = template.content.cloneNode(true);
    const inputs = clone.querySelectorAll('input');
    const buttons = clone.querySelectorAll('button');
    inputs[0].value = item.user.email;
    inputs[1].value = item.userDetails.name;
    inputs[2].value = item.userDetails.surname;

    //radio
    inputs[3].value = inputs[4].value = inputs[5].value = "f" + item.user.id;
    switch (item.userTypeText.userRoles){
        case "ROLE_ADMIN":
            inputs[3].checked = true;
            break;
        case "ROLE_USER":
            inputs[4].checked = true;
            break;
        case "ROLE_OFF":
            inputs[5].checked = true;
    }

    buttons[0].value = buttons[1].value = item.user.id;
    buttons[0].addEventListener("click", updateItem);
    buttons[1].addEventListener("click", deleteItem);
    itemsContainer.appendChild(clone);
}
