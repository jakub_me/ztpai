const itemsContainer = document.querySelector("#items");
const categoriesContainer = document.querySelector("#categories");
const name = document.querySelector('input[name=name]');
const editname = document.querySelector('#name');
const addRecord = document.querySelector("#addRecord");
const deleteButton = document.querySelector("#delete");
const renameButton = document.querySelector("#rename");
const editLink = document.querySelector('#edit-link');

let selectedDiv = null;

const buttons = document.querySelectorAll('.category-display');
buttons.forEach(button => button.addEventListener("click", selectCategory));
addRecord.addEventListener("click", addCategory);
deleteButton.addEventListener("click", deleteCategory);
renameButton.addEventListener("click", renameCategory);



function fetchTemplate(type,id="", body=null){
    if(body != null) body = JSON.stringify(body);
    return fetch(`/user/category/${id}`, {
        method: type,
        headers: {
            'Content-Type': 'application/json',
        },
        body: body
    })
}


function selectCategory() {
    selectedDiv = this.parentNode.parentNode;
    fetchTemplate("GET",this.value, null)
    .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
        return response.json();
    }).then(function(response){
        displaySwitchCategory(response.data)
    }).catch(function (e){
        console.log(e);
    });
}

function addCategory() {
    const data = {
        data: {
            name: name.value
        }
    }
    fetchTemplate("POST","", data)
    .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
        return response.json();
    }).then(function (category) {
        name.value = "";
        displayCategory(category.data);
    }).catch(function (e){
        console.log(e);
    });
}

function deleteCategory() {
    fetchTemplate("DELETE",this.value, null)
        .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
        console.log(response.status);
    }).then(function (category) {
        setButtons(true,true)
        editname.value = "";
        selectedDiv.parentNode.removeChild(selectedDiv);
        selectedDiv = null;
    }).catch(function (e){
        console.log(e);
    });
}

function renameCategory() {
    const data = {
        data: {
            name: editname.value
        }
    }
    fetchTemplate("PUT",this.value, data)
    .then(function (response) {
        if(!response.ok)
            throw Error(response.status.toString());
        return response.json();
    }).then(function (category) {
        editCategory(category.data);
    }).catch(function (e){
        console.log(e);
    });
}

function setButtons(r,d,id=null){
    document.querySelector('#rename').disabled=r;
    document.querySelector('#delete').disabled=d;
    if(id!==null){
        document.querySelector('#rename').value=id;
        document.querySelector('#delete').value=id;
    }
}

function displaySwitchCategory(category){
    editname.value = category.name;
    editLink.setAttribute('href', `useritems/${category.id}`);
    itemsContainer.innerHTML = "";
    if ('userItems' in category){
        category.userItems.forEach(item => displayItem(item));
        setButtons(false,false,category.id)
    }
    else setButtons(false,false,category.id)
}


function displayItem(item) {
    const template = document.querySelector("#items-template");
    const clone = template.content.cloneNode(true);
    const record = clone.querySelectorAll('td');
    record[0].innerHTML = item.name;
    record[1].innerHTML = item.amount;
    record[2].innerHTML = parseFloat(item.valuePerOne*0.01).toFixed(2);
    record[3].innerHTML = parseFloat(item.valuePerOne*item.amount*0.01).toFixed(2);
    itemsContainer.appendChild(clone);
}

function displayCategory(category) {
    const template = document.querySelector("#category-template");
    const clone = template.content.cloneNode(true);
    const record = clone.querySelectorAll('td');
    record[0].innerHTML = category.name;
    record[4].childNodes[0].value = category.id;
    record[4].childNodes[0].addEventListener("click", selectCategory);
    categoriesContainer.appendChild(clone);
}

function editCategory(category) {
    selectedDiv.querySelector('button').value = category.id;
    selectedDiv.querySelector('td').innerHTML = category.name;
    renameButton.value =  category.id;
    deleteButton.value =  category.id;
    editLink.setAttribute('href', `useritems/${category.id}`);
}
