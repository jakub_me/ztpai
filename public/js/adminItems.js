const itemsContainer = document.querySelector("tbody");
const categoryId = document.querySelector('input[type=hidden]');
const renameInput = document.querySelector("#category-name");

document.querySelector(".rename-category").addEventListener("click", renameCategory);
document.querySelector(".delete-category").addEventListener("click", deleteCategory);

document.querySelector(".add-item").addEventListener("click", addItem);
document.querySelectorAll(".delete-item").forEach(btn => btn.addEventListener("click", deleteItem));
document.querySelectorAll(".update-item").forEach(btn => btn.addEventListener("click", updateItem));

const buttons = document.querySelectorAll('.category-display');


function fetchTemplate(type, path, id = "", body = null) {
    if (body != null) body = JSON.stringify(body);
    return fetch(`/admin/${path}/${id}`, {
        method: type,
        headers: {
            'Content-Type': 'application/json',
        },
        body: body
    })
}

function renameCategory() {
    const data = {
        "data": {
            "name": renameInput.value
        }
    };
    fetchTemplate("POST", "category", categoryId.value, data)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
            return response.json();
        }).then(function (response) {
        categoryId.value = response.data.id;
    }).catch(function (e) {
        console.log(e);
    });
}

function deleteCategory() {
    fetchTemplate("DELETE", "category", categoryId.value, null)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
            window.location.href = `/admincatalog`;
        }).catch(function (e) {
        console.log(e);
    });
}


function addItem() {
    const record = this.parentNode.parentNode;
    const data = {
        "data": {
            "name": record.querySelector('input[name=name]').value,
            "value": parseFloat(record.querySelector('input[name=value]').value) * 100,
            "categoryId": categoryId.value
        }
    }
    fetchTemplate("POST", "items", "", data)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
            return response.json();
        }).then(function (item) {
        record.querySelector('#name-new').value = "";
        record.querySelector('#value-new').value = "";
        displayItem(item.data);
    }).catch(function (e) {
        console.log(e);
    });

}

function updateItem() {
    const record = this.parentNode.parentNode;
    const data = {
        "data": {
            "name": record.querySelector('input[name=name]').value,
            "value": parseFloat(record.querySelector('input[name=value]').value) * 100,
            "categoryId": categoryId.value
        }
    }
    fetchTemplate("PUT", "items", this.value, data)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
        }).catch(function (e) {
        console.log(e);
    });
}

function deleteItem() {
    const record = this.parentNode.parentNode;
    fetchTemplate("DELETE", "items", this.value, null)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
        }).then(function () {
        record.parentNode.removeChild(record);
    }).catch(function (e) {
        console.log(e);
    });
}


function displayItem(item) {
    const template = document.querySelector("#item-template");
    const clone = template.content.cloneNode(true);
    const record = clone.querySelectorAll('input');
    const buttons = clone.querySelectorAll('button');
    const value = (parseFloat(item.value) * 0.01).toFixed(2);
    record[0].setAttribute('value', item.name);
    record[1].setAttribute('value', value);
    buttons[0].value = item.id;
    buttons[1].value = item.id;
    buttons[0].addEventListener("click", updateItem);
    buttons[1].addEventListener("click", deleteItem);
    itemsContainer.appendChild(clone);
}
