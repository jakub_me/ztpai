const itemsContainer = document.querySelector("#items");
const categoriesContainer = document.querySelector("#categories");
const name = document.querySelector('#name');
const addRecord = document.querySelector("#add-record");
const form = document.querySelector("#form-add");


const buttons = document.querySelectorAll('.category-display');
buttons.forEach(button => button.addEventListener("click", selectCategory));
addRecord.addEventListener("click", addCategory);


function fetchTemplate(type, id = "", body = null) {
    if (body != null) body = JSON.stringify(body);
    return fetch(`/admin/category/${id}`, {
        method: type,
        headers: {
            'Content-Type': 'application/json',
        },
        body: body
    })
}

function selectCategory() {
    const id = this.getAttribute('data-url');
    fetchTemplate("GET", id, null)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
            return response.json();
        }).then(function (response) {
        displaySwitchCategory(response.data)
    }).catch(function (e) {
        console.log(e);
    });
}


function addCategory() {
    const data = {
        category: {
            name: form.querySelector('input[name=category]').value
        },
        item: {
            name: form.querySelector('input[name=itemName]').value,
            value: Math.floor(100 * parseFloat(form.querySelector('input[name=value]').value.replace(",", ".")))
        }
    }
    fetchTemplate("POST", "", data)
        .then(function (response) {
            if (!response.ok)
                throw Error(response.status.toString());
            return response.json();
        }).then(function (category) {
        displayCategory(category.data);
        displaySwitchCategory(category.data);
    }).catch(function (e) {
        console.log(e);
    });
}


function displayCategory(category) {
    const template = document.querySelector("#category-template");
    const clone = template.content.cloneNode(true);
    const record = clone.querySelectorAll('td');
    record[0].innerHTML = category.name;
    record[1].childNodes[0].setAttribute('data-url', category.id);
    record[1].childNodes[0].addEventListener("click", addCategory);
    categoriesContainer.appendChild(clone);
}

function displaySwitchCategory(category) {
    name.innerHTML = category.name;
    document.querySelector('#edit-link').setAttribute('href', `adminitems/${category.id}`);
    itemsContainer.innerHTML = "";
    category.adminItems.forEach(item => displayItem(item));
}

function displayItem(item) {
    const template = document.querySelector("#item-template");
    const clone = template.content.cloneNode(true);
    const record = clone.querySelectorAll('td');
    const value = (parseFloat(item.value) * 0.01).toFixed(2);
    record[0].innerHTML = item.name;
    record[1].innerHTML = value;
    itemsContainer.appendChild(clone);
}
