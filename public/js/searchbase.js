const itemsContainer = document.querySelector("#items");
const name = document.querySelector('#name');


const buttons = document.querySelectorAll('.category-display');
buttons.forEach(button => button.addEventListener("click", selectCategory));

const catId = localStorage.getItem('catId');
const catName = localStorage.getItem('catName');
const itemId = localStorage.getItem('itemId');
const itemName = localStorage.getItem('itemName');
if (catName === null)
    document.querySelector("#chosen-item").innerHTML = "Nie wybrano przedmiotu";
else document.querySelector("#chosen-item").innerHTML = "Wybrany przedmiot: " + itemName + " z kategorii " + catName;


function selectCategory() {
    const id = this.value;
    fetch(`/user/category-base/${id}`, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(function (response) {
        if (!response.ok)
            throw Error(response.status.toString());
        return response.json();
    }).then(function (response) {
        displaySwitchCategory(response.data)
    }).catch(function (e) {
        console.log(e);
    });
}


function displaySwitchCategory(category) {
    name.innerHTML = "Wybrana kategoria: " + category.name;
    itemsContainer.innerHTML = "";
    category.adminItems.forEach(item => displayItem(item));
}

function displayItem(item) {
    const template = document.querySelector("#item-template");
    const clone = template.content.cloneNode(true);
    const record = clone.querySelectorAll('td');
    const button = clone.querySelectorAll('button');
    const value = (parseFloat(item.value) * 0.01).toFixed(2);
    record[0].innerHTML = item.name;
    if (catName === null)
        button[1].disabled = true;
    record[1].innerHTML = value;
    button[0].value = item.id;//do schowka
    button[1].value = item.id;
    button[0].addEventListener("click", selectReference);
    button[1].addEventListener("click", setReferenceToItem);
    itemsContainer.appendChild(clone);
}


function selectReference() {
    localStorage.setItem("adminItemId", this.value);
    localStorage.setItem("adminItemName", this.parentNode.parentNode.firstElementChild.innerHTML);
}

function setReferenceToItem() {
    if (catName === null)
        return false;
    fetch(`/user/items/${itemId}/adminItem/${this.value}`, {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(function (response) {
        if (!response.ok)
            throw Error(response.status.toString());
        window.location.href = `/useritems/${catId}`;
    }).catch(function (e) {
        console.log(e);
    });
}