<?php


namespace App\Controller;


use App\Entity\Temporary\PasswordContainer;
use App\Entity\UserDetails;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    private $json;
    private $db;
    public function __construct(EntityManagerInterface $db)
    {
        $this->db = $db;
    }

    public function userpanel(){
        $id = $this->getUser()->getUsername();
        $userInfo = $this->getDoctrine()->getRepository(User::class)->findUserWithDetails($id);
        return $this->render('userpanel.html.twig',[
            "user" => $userInfo
        ]);
    }

    public function changeUserDetails(Request $request, ValidatorInterface $validator){
        $this->json = $this->db->find(UserDetails::class, $this->getUser()->getUsername());
        if(!$this->detailsValidate($request, $validator))
            return new JsonResponse(null,JsonResponse::HTTP_BAD_REQUEST);
        $this->db->flush();
        return new JsonResponse(null,JsonResponse::HTTP_NO_CONTENT);
    }

    public function changePassword(Request $request, ValidatorInterface $validator, UserPasswordEncoderInterface $passwordEncoder){
        if(!$this->passwordValidate($request, $validator))
            return new JsonResponse(null,JsonResponse::HTTP_BAD_REQUEST);

        $user = $this->db->find(User::class, $this->getUser()->getUsername());
        if(!$passwordEncoder->isPasswordValid($user, $this->json->getOldPassword()))
            return new JsonResponse(['error' => 'Old password is incorrect.'],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        $user->setPassword($passwordEncoder->encodePassword($user, $this->json->getNewPassword()));
        $this->db->flush();
        return new JsonResponse(['data' => 'Password changed.'],JsonResponse::HTTP_NO_CONTENT);
    }



    private function detailsValidate($request, $validator)
    {
        try{
            $parameters = json_decode($request->getContent(), true);
            $this->json->setName($parameters['data']['name']);
            $this->json->setSurname($parameters['data']['surname']);
            if(count($validator->validate($this->json)) > 0)
                return false;
        }
        catch(\Throwable $e) {
            return false;
        }
        return true;
    }

    private function passwordValidate($request, $validator)
    {
        try{
            $parameters = json_decode($request->getContent(), true);
            $this->json = new PasswordContainer($parameters);
            if(count($validator->validate($this->json)) > 0)
                return false;
        }
        catch(\Throwable $e) {
            return false;
        }
        return true;
    }
}