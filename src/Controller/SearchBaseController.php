<?php


namespace App\Controller;


use App\Entity\AdminItem;
use App\Entity\Category;
use App\Entity\UserItem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SearchBaseController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    public function searchbase() : Response
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->getBaseCategories();
        return $this->render('searchbase.html.twig', [
            'categories' => $categories,
        ]);
    }

    public function userSetAdminItemReference($id, $adminid){
        $db = $this->getDoctrine()->getManager();
        $adminItem = $db->find(AdminItem::class,$adminid);
        $userItem = $db->find(UserItem::class,$id);
        if(!$adminItem || !$userItem)
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        if($userItem->getIdUser()->getId() != $this->getUser()->getUsername())
            return new JsonResponse(null, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);

        $userItem->setAdminItem($adminItem);
        if($userItem->getAutoupdatable())
            $userItem->setValuePerOne($adminItem->getValue());
        $db->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}