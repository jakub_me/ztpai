<?php


namespace App\Controller;

use App\Entity\AdminItem;
use App\Entity\Category;
use App\Entity\Temporary\AdminItemNew;
use App\Entity\UserItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminItemsController extends AbstractController
{
    private $json;
    private $db;
    public function __construct(EntityManagerInterface $db)
    {
        $this->db = $db;
    }


    public function adminitems($categoryId) : Response
    {
        $items = $this->getDoctrine()->getRepository(Category::class)->getBaseItems($categoryId);
        if($items === [])
            return $this->redirectToRoute('admincatalog');
        foreach ($items[0]['adminItems'] as $item){
            $item['value'] = (float)$item['value'] * 0.01;
        }
        return $this->render('adminitems.html.twig', [
            'category' => $items[0],
        ]);
    }

    public function adminCategoryRename($id, Request $request) : Response
    {
        $parameters = json_decode($request->getContent(), true);
        if(!isset($parameters['data']['name']) || $parameters['data']['name'] == '')
            return new JsonResponse($parameters, JsonResponse::HTTP_BAD_REQUEST);
        //must be no other admin category with this name
        $category = $this->getDoctrine()->getRepository(Category::class)->getAdminCategory($parameters['data']['name']);
        //check if admin category already exists
        if ($category && !$category->getAdminItems()->isEmpty())
            return new JsonResponse(null, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);

        if ($category)
            $newId = $category->getId();
        else {
            $category = new Category();
            $category->setName($parameters['data']['name']);
            $this->db->persist($category);
            $this->db->flush();
            $newId = $this->db->getConnection()->lastInsertId();
        }
        //change all adminitems idcategory in this category
        $this->getDoctrine()->getRepository(AdminItem::class)->updateItemsIdCategory($id, $newId);
        return new JsonResponse(['data' =>
            ['name' => $category->getName(), 'id' => $newId]
        ]);
    }

    public function adminDeleteCategory($id) : Response
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        //check if admin category already exists
        if (!$category || $category->getAdminItems()->isEmpty())
            return new JsonResponse(null, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);

        //change all adminitems idcategory in this category
        $idArray = $this->getDoctrine()->getRepository(AdminItem::class)->selectItemsId($id);
        //user -> switch autoupdatable to false, reference to null - all matching objects
        $this->getDoctrine()->getRepository(UserItem::class)->deleteCategoryUserItemsReferences($idArray);
        //delete all admin objects in category
        $amount = $this->getDoctrine()->getRepository(AdminItem::class)->deleteAdminCategoryItems($category);
        //do not delete category - performance
        return new JsonResponse(['data' => ['amountDeleted' => $amount]],JsonResponse::HTTP_OK);
    }

    public function adminDeleteItem($id) : Response
    {
        $item = $this->db->getReference(AdminItem::class, $id);
        if(!$item)
            return new JsonResponse(null, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);

        $this->getDoctrine()->getRepository(UserItem::class)->deleteUserItemsReferences($id);
        $this->db->remove($item);
        $this->db->flush();
        return new JsonResponse(null,JsonResponse::HTTP_NO_CONTENT);
    }

    public function adminItemPut($id, Request $request, ValidatorInterface $validator) : Response
    {
        if(!$this->adminItemValidate($request, $validator))
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        $item = $this->getDoctrine()->getRepository(AdminItem::class)->find($id);
        //check if admin item exists
        if (!$item)
            return new JsonResponse(null, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);

        //update user values
        if($item->getValue() != $this->json->getValue())
            $this->getDoctrine()->getRepository(UserItem::class)->executeUpdateValues($id, $this->json->getValue());
        //update admin item
        $category = $this->getDoctrine()->getRepository(Category::class)->find($this->json->getCategoryId());
        $item->setName($this->json->getName());
        $item->setValue($this->json->getValue());
        $item->setIdCategory($category);
        $this->db->persist($item);
        try{
            $this->db->flush();
        }
        catch(\Throwable $e) {
            return new JsonResponse(['error' => 'Given category not exists.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
        $this->json->setId($id);
        return new JsonResponse(null,JsonResponse::HTTP_NO_CONTENT);
    }

    public function adminItemNew(Request $request,  ValidatorInterface $validator) : Response
    {
        if(!$this->adminItemValidate($request, $validator))
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);

        $item = new AdminItem();
        $category = $this->getDoctrine()->getRepository(Category::class)->find($this->json->getCategoryId());
        $item->setName($this->json->getName());
        $item->setValue($this->json->getValue());
        $item->setIdCategory($category);
        $this->db->persist($item);
        $this->db->flush();
        $this->json->setId($this->db->getConnection()->lastInsertId());
        return new JsonResponse(['data' => $this->json] ,JsonResponse::HTTP_CREATED);
    }

    private function adminItemValidate($request, $validator)
    {
        try{
            $parameters = json_decode($request->getContent(), true);
            $this->json = new AdminItemNew($parameters);
            if(count($validator->validate($this->json)) > 0)
                return false;
        }
        catch(\Throwable $e) {
            return false;
        }
        return true;
    }

}