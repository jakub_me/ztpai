<?php

namespace App\Controller;

use App\Form\Type\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser())
            return $this->redirectToRoute('userpanel');

        $error = $authenticationUtils->getLastAuthenticationError();
        if($error)
            $error = $error->getMessage();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('login.html.twig', [
            'form' => $this->createForm(LoginType::class)->createView(),
            'last_username' => $lastUsername, 'error' => $error]);
    }

    public function logout() {}
}
