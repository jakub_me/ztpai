<?php


namespace App\Controller;

use App\Entity\User;
use App\Entity\UserDetails;
use App\Entity\Temporary\UserRegister;
use App\Entity\UserTypes;
use App\Form\Type\RegisterType;
use App\Security\AppCustomAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class RegisterController extends AbstractController
{

    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder,
                             GuardAuthenticatorHandler $guardHandler,
                             AppCustomAuthenticator $authenticator): Response
    {
        if ($this->getUser())
            return $this->redirectToRoute('userpanel');

        $user = new UserRegister();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if (!$form->isSubmitted())
            return $this->render('register.html.twig', [
                'form' => $form->createView(),
                'error' => null
            ]);

        if (!$form->isValid())
            return $this->render('register.html.twig', [
                'form' => $form->createView(),
                'error' => 'Niepoprawny format danych.'
            ]);

        $user = $form->getData();
        $db = $this->getDoctrine()->getManager();
        if($this->getDoctrine()->getRepository(User::class)->findUserByEmail($user->getEmail()))
            return $this->render('register.html.twig', [
                'form' => $form->createView(),
                'error' => 'Uzytkownik o podanym mailu juz istnieje.'
            ]);

        $userDetails = UserDetails::RegisterUser(
            $user->getName(),
            $user->getSurname(),
        );
        $userMain = User::Register(
            $passwordEncoder,
            $user->getEmail(),
            $user->getPlainPassword(),
            $this->getDoctrine()->getRepository(UserTypes::class)->findOneByName('ROLE_USER'),
        );
        $userMain->setUserDetails($userDetails);

        $db->persist($userMain);
        $db->flush();
        $userMain->setId = $db->getConnection()->lastInsertId();

        return $guardHandler->authenticateUserAndHandleSuccess(
            $userMain,
            $request,
            $authenticator,
            'main' // firewall name in security.yaml
        );
    }

}
