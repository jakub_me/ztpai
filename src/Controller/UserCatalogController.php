<?php


namespace App\Controller;

use App\Entity\Category;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserCatalogController extends AbstractController
{
    private $name;
    private $db;
    public function __construct(EntityManagerInterface $db)
    {
        $this->db = $db;
    }

    public function usercatalog() : Response
    {
        $id = $this->getUser()->getUsername();
        $categories = $this->getDoctrine()->getRepository(Category::class)->findUserCategoriesDetailed($id) ?: [];
        return $this->render('usercatalog.html.twig', [
            'categoriesList' => $categories
        ]);
    }

    public function userDisplayCategoryDetails($categoryId){
        $uid = $this->getUser()->getUsername();
        $items = $this->getDoctrine()->getRepository(Category::class)->selectUserAndCategoryArray($uid, $categoryId);
        if (!$items)
            return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
        return new JsonResponse(['data' => $items[0]]);
    }

    public function userNewCategory(Request $request) : Response
    {
        if (!$this->userCategoryValidate($request))
            return new JsonResponse(['error' => 'Name must contain between 1 and 255 characters.'], JsonResponse::HTTP_BAD_REQUEST);
        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getUsername());
        $category = $this->getDoctrine()->getRepository(Category::class)->findCategoryByName($this->name);
        if (!$category) {
            $category = new Category();
            $category->setName($this->name);
        }
        else if(!$user->getCategories()->isEmpty() && $user->getCategories()->contains($category))
            return new JsonResponse(['error' => 'User already has this category.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        $category->addUsers($user);
        $this->db->persist($category);
        $this->db->flush();
        return new JsonResponse(['data' => [
            'name' => $this->name,
            'id' => $category->getId()
        ]], JsonResponse::HTTP_CREATED);
    }

    public function userDeleteCategory($id) : Response
    {
        $uid = $this->getUser()->getUsername();
        $user = $this->getDoctrine()->getRepository(User::class)->selectUserCategoryAndItems($uid, $id);
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        if (!$user)
            $user = $this->getDoctrine()->getRepository(User::class)->find($uid);
        else foreach ($user->getUserItems() as $item) {
            $user->removeUserItem($item);
        }
        try{
            $user->removeCategory($category);
            $this->db->flush();
            return new JsonResponse(null,JsonResponse::HTTP_NO_CONTENT);
        }
        catch(\Throwable $e) {
            return new JsonResponse(['error' => 'Nothing to delete.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function userRenameCategory($id, Request $request)
    {
        $emptyCategory = false;
        if (!$this->userCategoryValidate($request))
            return new JsonResponse(['error' => 'Name must contain between 1 and 255 characters.'], JsonResponse::HTTP_BAD_REQUEST);
        $uid = $this->getUser()->getUsername();
        $oldCategory = $this->getDoctrine()->getRepository(Category::class)->findCategoryOfUser($uid, $id);
        if(!$oldCategory)
            return new JsonResponse(['error' => 'User does not have given category to rename.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        $user = $this->getDoctrine()->getRepository(User::class)->selectUserAndItemsByIdCategory($uid, $id);
        if (!$user) {
            $emptyCategory = true;
            $user = $this->getDoctrine()->getRepository(User::class)->find($uid);
        }
        $newCategory = $this->getDoctrine()->getRepository(Category::class)->findCategoryByName($this->name);
        if($newCategory) {
            if ($user->getCategories()->contains($newCategory))
                return new JsonResponse(['error' => 'User already has category of goal name.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            $insertId = $newCategory->getId();
        }
        else {
            $newCategory = new Category();
            $newCategory->setName($this->name);
            $this->db->persist($newCategory);
            $this->db->flush();
            $insertId = $this->db->getConnection()->lastInsertId();
        }
        if(!$emptyCategory && !$user->getUserItems()->isEmpty())
            foreach ($user->getUserItems() as $item) {
                $item->setIdCategory($newCategory);
            }
        $user->addCategory($newCategory);
        $user->removeCategory($oldCategory);
        $this->db->flush();
        return new JsonResponse(['data'=> ['id' => $insertId, 'name' => $newCategory->getName()]]);
    }





    private function userCategoryValidate($request)
    {
        $parameters = json_decode($request->getContent(), true);
        if(!isset($parameters['data']['name']))
            return false;
        $this->name = $parameters['data']['name'];
        return $parameters['data']['name'] !== '' && strlen($parameters['data']['name']) < 255;
    }



}