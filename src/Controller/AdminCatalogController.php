<?php


namespace App\Controller;

use App\Entity\AdminItem;
use App\Entity\Category;
use App\Entity\Temporary\AdminCategoryNew;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminCatalogController extends AbstractController
{
    private $json;
    private $db;
    public function __construct(EntityManagerInterface $db)
    {
        $this->db = $db;
    }

    public function admincatalog(): Response
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAllAdminCategories();
        return $this->render('admincatalog.html.twig', [
            'catalogs' => $categories,
            'error' => null,
            'text' => null
        ]);
    }

    public function adminDisplayCategoryDetails($id)
    {
        $items = $this->getDoctrine()->getRepository(Category::class)->getBaseItems($id);
        if ($items)
            return new JsonResponse(['data' => $items[0]]);
        return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
    }

    public function adminAddCategoryWithItem(Request $request, ValidatorInterface $validator)
    {
        if(!$this->newAdminCategoryValidate($request, $validator))
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);

        $category = $this->getDoctrine()->getRepository(Category::class)->getAdminCategory($this->json->getCategoryName());
        //check if admin category already exists
        if ($category && !$category->getAdminItems()->isEmpty())
            return new JsonResponse(null, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        if ($category)
            $id = $category->getId();
        else {
            $category = $this->json->createCategoryItem();
            $this->db->persist($category);
            $this->db->flush();
            $id = $this->db->getConnection()->lastInsertId();
        }
        $item = $this->json->createAdminItem();
        $item->setIdCategory($category);
        $this->db->persist($item);
        $this->db->flush();
        return $this->redirectToRoute('adminDisplayCategoryDetails', ['id' => $id]);
    }

    private function newAdminCategoryValidate($request, $validator)
    {
        try{
            $parameters = json_decode($request->getContent(), true);
            $this->json = new AdminCategoryNew($parameters);
            if(count($validator->validate($this->json)) > 0)
                return false;
        }
        catch(\Throwable $e) {
            return false;
        }
        return true;
    }

}