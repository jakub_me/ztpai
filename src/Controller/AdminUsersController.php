<?php


namespace App\Controller;


use App\Entity\Temporary\UserAddByAdmin;
use App\Entity\Temporary\UserPutByAdmin;
use App\Entity\User;
use App\Entity\UserDetails;
use App\Entity\UserTypes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminUsersController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    private $json;
    private $db;
    public function __construct(EntityManagerInterface $db)
    {
        $this->db = $db;
    }
    public function adminusers() : Response
    {
        $list = $this->getDoctrine()->getRepository(User::class)->adminGetListOfUsers();
        return $this->render('adminusers.html.twig', [
            'list' => $list,
            'error' => ''
        ]);
    }

    public function adminUserPut($id, Request $request, ValidatorInterface $validator, UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        if(!$this->entityValidate($request, $validator, $passwordEncoder, "App\Entity\Temporary\UserPutByAdmin"))
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if(!$user)
            return new JsonResponse(['error' => 'Given user not exists!'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->json->setUser($user);
        try{
            $this->db->flush();
        }
        catch(\Throwable $e) {
            return new JsonResponse(['error' => 'User with such email already exists!'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    public function adminUserDel($id, Request $request) : Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if(!$user)
            return new JsonResponse(['error' => 'Given user not exists!'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        $this->db->remove($user);
        $this->db->flush();
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    public function adminUserNew(Request $request, ValidatorInterface $validator, UserPasswordEncoderInterface $passwordEncoder) : Response
    {
        if(!$this->entityValidate($request, $validator, $passwordEncoder))
            return new JsonResponse(null, JsonResponse::HTTP_BAD_REQUEST);
        $userDetails = $this->json->createUserDetails();
        $this->db->persist($userDetails);
        $this->db->flush();
        $detId = $this->db->getConnection()->lastInsertId();
        $this->json->setIdDetails($detId);
        $user = $this->json->createUser();
        $this->db->persist($user);
        try{
            $this->db->flush();
            $this->json->setId($this->db->getConnection()->lastInsertId());
        }
        catch(\Throwable $e) {
            $this->db = $this->getDoctrine()->resetManager();
            $userDetails = $this->db->getReference(UserDetails::class, $detId);
            $this->db->remove($userDetails);
            $this->db->flush();
            return new JsonResponse(['error' => 'User with such email already exists!'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
        return new JsonResponse(['data' => $this->json]);
    }



    private function entityValidate($request, $validator, $passwordEncoder, $entity = "App\Entity\Temporary\UserAddByAdmin")
    {
        try{
            $parameters = json_decode($request->getContent(), true);
            $this->json = new $entity($parameters, $passwordEncoder);
            if(count($validator->validate($this->json)) > 0)
                return false;
            $userType = $this->getDoctrine()->getRepository(UserTypes::class)->findOneByName($this->json->getUserType());
            if(!$userType)
                return false;
        }
        catch(\Throwable $e) {
            return false;
        }
        $this->json->setUserType($userType);
        return true;
    }
}