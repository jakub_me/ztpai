<?php


namespace App\Controller;

use App\Entity\AdminItem;
use App\Entity\Category;
use App\Entity\User;
use App\Entity\UserItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserItemsController extends AbstractController
{
    private $db;
    public function __construct(EntityManagerInterface $db)
    {
        $this->db = $db;
    }

    public function useritems($categoryId) : Response
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->selectUserAndCategory($this->getUser()->getUsername(), $categoryId);
        if(!$category)
            $this->redirectToRoute('usercatalog');
        if(!isset($category[0]['userItems'])) $category[0]['userItems'] = [];
        return $this->render('useritems.html.twig', [
            'category' => $category[0],
        ]);
    }




    public function userDeleteItem($id) : Response
    {
        $uid = $this->getUser()->getUsername();
        $user = $this->getDoctrine()->getRepository(User::class)->selectUserItem($uid, $id);
        if (!$user)
            return new JsonResponse(['error' => 'Nothing to delete.'], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        $user->removeUserItem($user->getUserItems()[0]);
        $this->db->flush();
        return new JsonResponse(null,JsonResponse::HTTP_NO_CONTENT);
    }

    public function userNewItem(Request $request, ValidatorInterface $validator)
    {
        $adminItem = null;
        try {
            $parameters = json_decode($request->getContent(), true);
            $this->json = new UserItem();
            $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getUsername());
            if (isset($parameters['data']['adminItemId'])){
                $adminItem = $this->getDoctrine()->getRepository(AdminItem::class)->find($parameters['data']['adminItemId']);
                if(!$adminItem)
                    return new JsonResponse(['error' => 'Admin item not exists.'],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            if(!$user)  return new JsonResponse(['error' => $user->getCategories()],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            foreach ($user->getCategories() as $cat){
                if($cat->getId() == $parameters['data']['categoryId']){
                    $category = $cat;
                    break;
                }
            }
            //$category = $user->getCategories()->($parameters['data']['categoryId']);
            if(!isset($category))
                return new JsonResponse(['error' => 'User does not have such category.'],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            $this->json->setIdUser($user);
            $this->json->setIdCategory($category);
            $this->json->setAdminItem($adminItem);
            $this->json->setName($parameters['data']['name']);
            $this->json->setAmount($parameters['data']['amount']);
            $this->json->setValuePerOne($parameters['data']['autoupdatable'] ? $adminItem->getValue() : $parameters['data']['valuePerOne']);
            $this->json->setAutoupdatable($parameters['data']['autoupdatable']);
            if (count($validator->validate($this->json)) > 0)
                return new JsonResponse(['error' => $validator->validate($this->json)[0]],JsonResponse::HTTP_BAD_REQUEST);

            $this->db->persist($this->json);
            $this->db->flush();
            $this->json->setId($this->db->getConnection()->lastInsertId());
            return new JsonResponse(['data' => $this->json->jsonSerialize()]);
        } catch (\Throwable $e) {
            return new JsonResponse(['error' => $e->getMessage()],JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    public function userPutItem($id, Request $request, ValidatorInterface $validator)
    {
        $uid = $this->getUser()->getUsername();
        $adminItem = null;
        try{
            $user = $this->getDoctrine()->getRepository(User::class)->find($uid);
            $parameters = json_decode($request->getContent(), true);
            if($parameters['data']['autoupdatable'] && !isset($parameters['data']['adminItemId']))
                return new JsonResponse(['error' => 'Requested autoupdatable value but no admin item given.'],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            $userItem = $this->getDoctrine()->getRepository(UserItem::class)->find($id);
            if(!$userItem)
                return new JsonResponse(['error' => 'No such item found.'],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            if((isset($parameters['data']['userId']) && $parameters['data']['userId'] != $uid)
                || $userItem->getIdUser()->getId() != $uid)
                return new JsonResponse(['error' => "User cannot be changed."],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            if(isset($parameters['data']['adminItemId'])){
                $adminItem = $this->getDoctrine()->getRepository(AdminItem::class)->find($parameters['data']['adminItemId']);
                if(!$adminItem)
                    return new JsonResponse(['error' => "Given admin item not exists."],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            }
            $userItem->setAdminItem($adminItem);
            foreach ($user->getCategories() as $cat){
                if($cat->getId() == $parameters['data']['categoryId']){
                    $category = $cat;
                    break;
                }
            }
            //$category = $user->getCategories()->get($parameters['data']['categoryId']);
            if(!$category)
                return new JsonResponse(['error' => "User does not have such category."],JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
            $userItem->setIdCategory($category);
            $userItem->setName($parameters['data']['name']);
            $userItem->setAmount($parameters['data']['amount']);
            $userItem->setValuePerOne($parameters['data']['autoupdatable'] ? $adminItem->getValue() : $parameters['data']['valuePerOne']);
            $userItem->setAutoupdatable($parameters['data']['autoupdatable']);
            if(count($validator->validate($userItem)) > 0)
                return new JsonResponse(null,JsonResponse::HTTP_BAD_REQUEST);
            $this->db->flush();
            return new JsonResponse(null,JsonResponse::HTTP_NO_CONTENT);
        }
        catch(\Throwable $e) {
            return new JsonResponse(null,JsonResponse::HTTP_BAD_REQUEST);
        }
    }
}