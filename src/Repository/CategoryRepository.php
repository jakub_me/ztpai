<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }


    public function findCategoryByName($name): ?Category
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.name = :val')
            ->setParameter('val', $name)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findCategoryOfUser($userId, $catId): ?Category
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.id = :cat')
            ->join('u.users','us')
            ->andWhere('us.id = :user')
            ->setParameter('cat', $catId)
            ->setParameter('user', $userId)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
    public function findAllAdminCategories()
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->distinct()
            ->join('c.adminItems', 'a')
            ->getQuery()
            ->getResult();
    }
    public function findUserCategoriesDetailed($id)
    {
        return $this->createQueryBuilder('c')
            ->select('c.id as catId', 'c.name as catName')
            ->join('c.users', 'u')
            ->leftJoin('c.userItems', 'i', 'WITH', 'i.idUser = :id')
            ->where('u.id = :id')
            //->andWhere('i.idUser = :id')
            ->groupBy('c.id, c.name')
            ->addSelect('SUM(i.valuePerOne * i.amount) as totalValue')
            ->addSelect('SUM(coalesce(i.amount,0)) as totalAmount')
            ->addSelect('COUNT(i.id) as totalRecords')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult();
    }

    public function selectUserAndCategory($idUser, $idCat)
    {
        $array = $this->createQueryBuilder('c')
            ->select(['c','i','a'])
            ->where('c.id = :cat')
            ->join('c.userItems','i')
            ->join('i.idUser','u')
            ->andWhere('u.id = :user')
            ->leftJoin('i.adminItem', 'a')
            ->setParameter('user', $idUser)
            ->setParameter('cat', $idCat)
            ->getQuery()
            ->getArrayResult();
        if($array)
            return $array;
        return $this->createQueryBuilder('c')
            ->select(['c'])
            ->where('c.id = :cat')
            ->join('c.users','u')
            ->andWhere('u.id = :user')
            ->setParameter('user', $idUser)
            ->setParameter('cat', $idCat)
            ->getQuery()
            ->getArrayResult();
    }

    public function selectUserAndCategoryArray($idUser, $idCat)
    {
        $array = $this->createQueryBuilder('c')
            ->select(['c','i'])
            ->where('c.id = :cat')
            ->join('c.userItems','i')
            ->andWhere('i.idUser = :user')
            ->join('i.idUser','u')
            ->andWhere('u.id = :user')
            ->setParameter('user', $idUser)
            ->setParameter('cat', $idCat)
            ->getQuery()
            ->getArrayResult();
        if($array)
            return $array;
        return $this->createQueryBuilder('c')
            ->select(['c'])
            ->where('c.id = :cat')
            ->join('c.users','u')
            ->andWhere('u.id = :user')
            ->setParameter('user', $idUser)
            ->setParameter('cat', $idCat)
            ->getQuery()
            ->getArrayResult();
    }

    public function getBaseCategories()
    {
        return $this->createQueryBuilder('c')
            ->select('c.id as catId', 'c.name as catName')
            ->join('c.adminItems', 'a')
            ->groupBy('c.id, c.name')
            ->addSelect('COUNT(a.id) as totalRecords')
            ->getQuery()
            ->getResult();
    }
    public function getBaseItems($catId)
    {
        return $this->createQueryBuilder('c')
            ->select(['c','i'])
            ->join('c.adminItems', 'i')
            ->andWhere('c.id = :id')
            ->setParameter('id', $catId)
            ->getQuery()
            ->getArrayResult()
            ;
    }
    public function getAdminCategory($catName)
    {
        $c =  $this->createQueryBuilder('c')
            ->select(['c','i'])
            ->leftJoin('c.adminItems', 'i')
            ->andWhere('c.name = :name')
            ->setParameter('name', $catName)
            ->getQuery()
            ->getOneOrNullResult()
            ;
        if($c)
            return $c;
        return $this->createQueryBuilder('c')
            ->select(['c'])
            ->andWhere('c.name = :name')
            ->setParameter('name', $catName)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
