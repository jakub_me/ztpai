<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function loadUserByUsername($emailOrId)
    {
        return $this->createQuery(
            'SELECT u
                FROM App\Entity\User u
                WHERE u.id = :query
                OR u.email = :query'
        )
            ->setParameter('query', $emailOrId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findUserByEmail($email): ?User
    {
        return $this->createQueryBuilder('u')
            ->select(['u','t'])
            ->where('u.email = :val')
            ->join('u.userType','t')
            ->setParameter('val', $email)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    public function findUserWithDetails($id): ?User
    {
        return $this->createQueryBuilder('u')
            ->select(['u','d'])
            ->where('u.id = :val')
            ->join('u.userDetails','d')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function adminGetListOfUsers()
    {
        return $this->createQueryBuilder('u')
            ->select(['u', 'd', 't'])
            ->join('u.userDetails', 'd')
            ->join('u.userType', 't')
            ->getQuery()
            ->getResult()
            ;
    }

    public function selectUserCategoryAndItems($idUser, $idCat): ?User
    {
        return $this->createQueryBuilder('u')
            ->select(['u','i'])
            ->where('u.id = :user')
            ->join('u.userItems','i')
            ->join('i.idCategory','c')
            ->andWhere('c.id = :cat')
            //->andWhere('c.users = :user')
            ->setParameter('user', $idUser)
            ->setParameter('cat', $idCat)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function selectUserAndItemsByIdCategory($idUser, $idCat): ?User
    {
        return $this->createQueryBuilder('u')
            ->select(['u','i'])
            ->where('u.id = :user')
            ->join('u.userItems','i')
            ->join('i.idCategory','c', 'WITH')
            ->andWhere('c.id = :cat')
            ->setParameter('user', $idUser)
            ->setParameter('cat', $idCat)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function selectUserItem($idUser, $idItem): ?User
    {
        return $this->createQueryBuilder('u')
            ->select(['u','i'])
            ->where('u.id = :user')
            ->join('u.userItems','i')
            ->andWhere('i.id = :id')
            ->setParameter('user', $idUser)
            ->setParameter('id', $idItem)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
