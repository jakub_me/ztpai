<?php

namespace App\Repository;

use App\Entity\AdminItem;
use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AdminItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdminItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdminItem[]    findAll()
 * @method AdminItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdminItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdminItem::class);
    }

    public function findAdminItem($name, $catId): ?AdminItem
    {
        return $this->createQueryBuilder('u')
            ->where('u.name = :val')
            ->andWhere('u.idCategory = :id')
            ->setParameter('val', $name)
            ->setParameter('id', $catId)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function updateItemsIdCategory($oldId, $newId)
    {
        return $this->createQueryBuilder('a')
            ->update()
            ->set('a.idCategory', ':newId')
            ->where('a.idCategory = :oldId')
            ->setParameter('oldId', $oldId)
            ->setParameter('newId', $newId)
            ->getQuery()
            ->execute();
    }
    public function selectItemsId($categoryId)
    {
        $result = $this->createQueryBuilder('a')
            ->select('a.id')
            ->where('a.idCategory = :id')
            ->setParameter('id', $categoryId)
            ->getQuery()
            ->getScalarResult();
        return array_column($result, "id");
    }

    public function deleteAdminCategoryItems($category)
    {
        return $this->createQueryBuilder('u')
            ->delete()
            ->where('u.idCategory = :id')
            ->setParameter('id', $category)
            ->getQuery()
            ->execute();
    }
}
