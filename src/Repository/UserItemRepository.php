<?php

namespace App\Repository;

use App\Entity\UserItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserItem[]    findAll()
 * @method UserItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserItem::class);
    }

    public function findUserItem($name, $catId): ?UserItem
    {
        return $this->createQueryBuilder('u')
            ->where('u.name = :val')
            ->andWhere('u.idCategory = :id')
            ->setParameter('val', $name)
            ->setParameter('id', $catId)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }


    public function deleteCategoryUserItemsReferences($adminItemsIdArray)
    {
        return $this->createQueryBuilder('u')
            ->update()
            ->set('u.adminItem', ':newId')
            ->set('u.autoupdatable', ':update')
            ->where('u.adminItem IN (:oldIds)')
            ->setParameter('update', false)
            ->setParameter('newId', null)
            ->setParameter('oldIds', $adminItemsIdArray)
            ->getQuery()
            ->execute();
    }
    public function deleteUserItemsReferences($adminItemsId)
    {
        return $this->createQueryBuilder('u')
            ->update()
            ->set('u.adminItem', ':newId')
            ->set('u.autoupdatable', ':update')
            ->where('u.adminItem = :oldId')
            ->setParameter('update', false)
            ->setParameter('newId', null)
            ->setParameter('oldId', $adminItemsId)
            ->getQuery()
            ->execute();
    }
    public function executeUpdateValues($adminItemsId, $value)
    {
        return $this->createQueryBuilder('u')
            ->update()
            ->set('u.valuePerOne', ':val')
            ->where('u.autoupdatable = :bool')
            ->andWhere('u.adminItem = :id')
            ->setParameter('val', $value)
            ->setParameter('id', $adminItemsId)
            ->setParameter('bool', true)
            ->getQuery()
            ->execute();
    }

}
