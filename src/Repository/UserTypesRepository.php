<?php

namespace App\Repository;

use App\Entity\UserTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserTypes[]    findAll()
 * @method UserTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserTypes::class);
    }


    public function findOneByName($value): ?UserTypes
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.name = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
