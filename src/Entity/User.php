<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface as e;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields="email", message="That email is taken!")
 */
class User implements UserInterface, \Serializable, EquatableInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique = true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToOne(targetEntity=UserDetails::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_details_id", referencedColumnName="id", nullable=false)
     */
    private $userDetails;
    /**
     * @ORM\ManyToOne(targetEntity=UserTypes::class, cascade={"persist"},  inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userType;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="users")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=UserItem::class, mappedBy="idUser", orphanRemoval=true)
     */
    private $userItems;

    private $plainPassword;

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
        $this->password = null;
    }

    public function __construct($id = null)
    {
        $this->categories = new ArrayCollection();
        $this->userItems = new ArrayCollection();
        $this->id = $id;
    }

    public static function Register(e $encoder, string $email, string $plainPass, ?UserTypes $setRole = null)
    {
        $user = new User();
        $user->setEmail($email);
        $user->setPlainPassword($plainPass);
        $user->setUserType($setRole);
        $user->setPassword($encoder->encodePassword($user, $plainPass));
        return $user;
    }

    public function getRoles()
    {
        $x = $this->getUserType();
        return is_null($x) ? ['ROLE_GUEST'] : [$x->getName()] ;
    }

    public function getUsername()
    {
        return $this->id;
    }
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    public function getSalt()
    {
        ;
    }



    //other functions

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getUserDetails(): ?UserDetails
    {
        return $this->userDetails;
    }

    public function setUserDetails(UserDetails $userDetails): self
    {
        $this->userDetails = $userDetails;

        return $this;
    }

    public function getUserType(): ?UserTypes
    {
        return $this->userType;
    }

    public function setUserType(?UserTypes $userType): self
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addUsers($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeUsers($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserItem[]
     */
    public function getUserItems(): Collection
    {
        return $this->userItems;
    }

    public function addUserItem(UserItem $userItem): self
    {
        if (!$this->userItems->contains($userItem)) {
            $this->userItems[] = $userItem;
            $userItem->setIdUser($this);
        }

        return $this;
    }

    public function removeUserItem(UserItem $userItem): self
    {
        if ($this->userItems->removeElement($userItem)) {
            // set the owning side to null (unless already changed)
            if ($userItem->getIdUser() === $this) {
                $userItem->setIdUser(null);
            }
        }

        return $this;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->id,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->id,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function isEqualTo(UserInterface $user)
    {
        return $this->getId() == intval($user->getUsername());
    }
}
