<?php

namespace App\Entity;

use App\Repository\UserItemRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserItemRepository::class)
 */
class UserItem implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userItems")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $idUser;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(min=3,max=100)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     */
    private $amount;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotNull()
     */
    private $valuePerOne;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $autoupdatable;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="userItems")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     */
    private $idCategory;

    /**
     * @ORM\ManyToOne(targetEntity=AdminItem::class, inversedBy="userItems")
     */
    private $adminItem;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getValuePerOne(): ?int
    {
        return $this->valuePerOne;
    }

    public function setValuePerOne(int $valuePerOne): self
    {
        $this->valuePerOne = $valuePerOne;

        return $this;
    }

    public function getAutoupdatable(): ?bool
    {
        return $this->autoupdatable;
    }

    public function setAutoupdatable(?bool $autoupdatable): self
    {
        $this->autoupdatable = $autoupdatable;

        return $this;
    }

    public function getIdCategory(): ?Category
    {
        return $this->idCategory;
    }

    public function setIdCategory(?Category $idCategory): self
    {
        $this->idCategory = $idCategory;

        return $this;
    }

    public function getAdminItem(): ?AdminItem
    {
        return $this->adminItem;
    }

    public function setAdminItem(?AdminItem $adminItem): self
    {
        $this->adminItem = $adminItem;

        return $this;
    }
    public function jsonSerialize()
    {
        $aItem = isset($this->adminItem) && $this->adminItem->getId() ? $this->adminItem->getId() : null;
        $aName = $aItem ? $this->adminItem->getName() : null;
        return [
            'id' => $this->id,
            'categoryId' => $this->idCategory->getId(),
            'valuePerOne' => $this->valuePerOne,//number_format($this->valuePerOne/100., 2, '.', ''),
            'amount' =>$this->amount,
            'adminItemId' => $aItem,
            'adminItemName' => $aName,
            'autoupdatable' => $this->autoupdatable,
            'name' => $this->name,
        ];
    }
}
