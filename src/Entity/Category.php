<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="categories")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=AdminItem::class, mappedBy="idCategory", orphanRemoval=true)
     */
    private $adminItems;

    /**
     * @ORM\OneToMany(targetEntity=UserItem::class, mappedBy="idCategory", orphanRemoval=true)
     */
    private $userItems;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->adminItems = new ArrayCollection();
        $this->userItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUsers(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUsers(User $user): self
    {
        $this->users->removeElement($user);

        return $this;
    }

    /**
     * @return Collection|AdminItem[]
     */
    public function getAdminItems(): Collection
    {
        return $this->adminItems;
    }

    public function addAdminItem(AdminItem $adminItem): self
    {
        if (!$this->adminItems->contains($adminItem)) {
            $this->adminItems[] = $adminItem;
            $adminItem->setIdCategory($this);
        }

        return $this;
    }

    public function removeAdminItem(AdminItem $adminItem): self
    {
        if ($this->adminItems->removeElement($adminItem)) {
            // set the owning side to null (unless already changed)
            if ($adminItem->getIdCategory() === $this) {
                $adminItem->setIdCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserItem[]
     */
    public function getUserItems(): ?Collection
    {
        return $this->userItems;
    }

    public function addUserItem(UserItem $userItem): self
    {
        if (!$this->userItems->contains($userItem)) {
            $this->userItems[] = $userItem;
            $userItem->setIdCategory($this);
        }

        return $this;
    }

    public function removeUserItem(UserItem $userItem): self
    {
        if ($this->userItems->removeElement($userItem)) {
            // set the owning side to null (unless already changed)
            if ($userItem->getIdCategory() === $this) {
                $userItem->setIdCategory(null);
            }
        }

        return $this;
    }
}
