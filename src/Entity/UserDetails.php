<?php

namespace App\Entity;

use App\Repository\UserDetailsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserDetailsRepository::class)
 */
class UserDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     * @ORM\OneToOne(targetEntity="User", inversedBy="userDetails")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\Length(min=2,max=30)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Length(min=2,max=40)
     */
    private $surname;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }
    public static function RegisterUser(string $name, string $surname)
    {
        $user = new UserDetails();
        $user->setName($name);
        $user->setSurname($surname);
        return $user;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

}
