<?php


namespace App\Entity\Temporary;

use Symfony\Component\Validator\Constraints as Assert;

class PasswordContainer
{
    /**
     * @Assert\Length(min=2,max=255)
     */
    private string $oldPassword;
    /**
     * @Assert\Length(min=2,max=255)
     */
    private string $newPassword;

    public function __construct(array $data)
    {
        $this->oldPassword = $data['data']['oldPassword'];
        $this->newPassword = $data['data']['newPassword'];
    }

    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    public function getNewPassword()
    {
        return $this->newPassword;
    }

}