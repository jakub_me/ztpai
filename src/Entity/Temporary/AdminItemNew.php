<?php


namespace App\Entity\Temporary;


use App\Entity\AdminItem;
use Symfony\Component\Validator\Constraints as Assert;

class AdminItemNew  implements \JsonSerializable
{
    /**
     * @Assert\NotNull()
     */
    private string $categoryId;
    /**
     * @Assert\NotNull()
     * @Assert\Length(min=3,max=255)
     */
    private string $name;
    /**
     * @Assert\NotNull()
     */
    private int $value;
    //for returning an object
    private int $itemId;

    public function __construct(array $data)
    {
        $this->categoryId = $data['data']['categoryId'];
        $this->name = $data['data']['name'];
        $val = $data['data']['value'];
        $this->value = $val ?: 0; // float value to int
    }

    public function createAdminItem(): AdminItem
    {
        $item = new AdminItem();
        $item->setName($this->name);
        $item->setValue($this->value);
        return $item;
    }


    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setId($itemId)
    {
       $this->itemId = $itemId;
    }

    public function jsonSerialize()
    {
        return [
            'categoryId' => $this->categoryId,
            'value' => $this->value,//number_format($this->value/100., 2, '.', ''),
            'name' => $this->name,
            'id' => $this->itemId
        ];
    }

}