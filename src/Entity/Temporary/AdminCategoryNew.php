<?php


namespace App\Entity\Temporary;


use App\Entity\AdminItem;
use App\Entity\Category;
use Symfony\Component\Validator\Constraints as Assert;

class AdminCategoryNew
{
    /**
     * @Assert\NotNull()
     * @Assert\Length(min=3,max=255)
     */
    private string $categoryName;
    /**
     * @Assert\NotNull()
     * @Assert\Length(min=1,max=255)
     */
    private string $name;
    /**
     * @Assert\NotNull()
     */
    private int $value;

    public function __construct(array $data)
    {
        $this->categoryName = $data['category']['name'];
        $this->name = $data['item']['name'];
        $val = $data['item']['value'];
        $this->value = $val ?: 0;
    }

    public function createCategoryItem(): Category
    {
        $item = new Category();
        $item->setName($this->categoryName);
        return $item;
    }

    public function createAdminItem(): AdminItem
    {
        $item = new AdminItem();
        $item->setName($this->name);
        $item->setValue($this->value);
        return $item;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): int
    {
        return $this->value;
    }

}