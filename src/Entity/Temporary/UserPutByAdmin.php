<?php


namespace App\Entity\Temporary;


use App\Entity\User;
use App\Entity\UserDetails;
use App\Entity\UserTypes;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use function Symfony\Component\Translation\t;

class UserPutByAdmin
{
    /**
     * @Assert\Length(min=1,max=30)
     */
    private string $name;
    /**
     * @Assert\Length(min=1,max=40)
     */
    private string $surname;
    /**
     * @Assert\NotNull()
     */
    private string $userTypeText;
    /**
     * @Assert\Length(min=1,max=100)
     */
    private string $email;

    private int $id;
    private int $idUserDetails;

    //built entities
    private ?UserTypes $userType;
    private User $user;


    private $passwordEncoder;

    public function __construct(array $data, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->name = $data['data']['userDetails']['name'];
        $this->surname = $data['data']['userDetails']['surname'];
        $this->userTypeText = $data['data']['userRoles']['userTypeText'];
        $this->email = $data['data']['user']['email'];
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getUserType()
    {
        return $this->userTypeText;
    }

    public function setUserType(?UserTypes $userType): void
    {
        $this->userType = $userType;
    }


    public function setUser(User $user)
    {
        $user->setUserType($this->userType);
        $user->getUserDetails()->setName($this->name);
        $user->getUserDetails()->setSurname($this->surname);
        $user->setEmail($this->email);
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdUserDetails()
    {
        return $this->idUserDetails;
    }


}