<?php


namespace App\Entity\Temporary;

class UserRegister
{
    protected $email = null;
    protected $name = null;
    protected $surname = null;
    protected $plainPassword = null;
    protected $submit = null;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function print():string
{
    return $this->email.' '.$this->name.' '.$this->surname.' '.$this->plainPassword;
}

    public function getSubmit()
    {
        return $this->submit;
    }

    public function setSubmit($submit): void
    {
        $this->submit = $submit;
    }


}