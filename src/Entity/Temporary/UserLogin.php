<?php


namespace App\Entity\Temporary;

class UserLogin
{
    protected $email = null;
    protected $plainPassword = null;
    protected $submit = null;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getSubmit()
    {
        return $this->submit;
    }

    public function setSubmit($submit): void
    {
        $this->submit = $submit;
    }


}