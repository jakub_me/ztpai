<?php


namespace App\Entity\Temporary;


use App\Entity\User;
use App\Entity\UserDetails;
use App\Entity\UserTypes;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserAddByAdmin  implements \JsonSerializable
{
    /**
     * @Assert\Length(min=1,max=255)
     */
    private string $plainPassword;
    /**
     * @Assert\Length(min=1,max=30)
     */
    private string $name;
    /**
     * @Assert\Length(min=1,max=40)
     */
    private string $surname;
    /**
     * @Assert\NotNull()
     */
    private string $userTypeText;
    /**
     * @Assert\Length(min=1,max=100)
     */
    private string $email;

    //built entities
    private ?UserTypes $userType;
    private UserDetails $userDetails;
    private User $user;

    //for returning an object
    private int $id;
    private int $idUserDetails;

    private $passwordEncoder;

    public function __construct(array $data, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->plainPassword = $data['data']['user']['plainPassword'];
        $this->name = $data['data']['userDetails']['name'];
        $this->surname = $data['data']['userDetails']['surname'];
        $this->userTypeText = $data['data']['userRoles']['userTypeText'];
        $this->email = $data['data']['user']['email'];
        $this->passwordEncoder = $passwordEncoder;
    }

    public function getUserType()
    {
        return $this->userTypeText;
    }

    public function setUserType(?UserTypes $userType): void
    {
        $this->userType = $userType;
    }

    public function createUserDetails()
    {
        $this->userDetails = new UserDetails();
        $this->userDetails->setSurname($this->surname);
        $this->userDetails->setName($this->name);
        return $this->userDetails;
    }

    public function createUser(): User
    {
        $this->setUser();
        $this->user->setUserDetails($this->userDetails);
        $this->user->setUserType($this->userType);
        return $this->user;
    }

    private function setUser()
    {
        $this->user = new User();
        $this->user->setEmail($this->email);
        $this->user->setPassword($this->passwordEncoder->encodePassword($this->user, $this->plainPassword));
    }

    public function setId($itemId)
    {
        $this->id = $itemId;
    }

    public function setIdDetails($itemId)
    {
        $this->idUserDetails = $itemId;
    }

    public function jsonSerialize()
    {
        $user = [
            'id' => $this->id,
            'email' => $this->email
        ];
        $userDetails = [
            'id' => $this->idUserDetails,
            'name' => $this->name,
            'surname' => $this->surname
        ];
        return [
            'user' => $user,
            'userDetails' => $userDetails,
            'userTypeText' => ['userRoles' => $this->userType->getName()],
        ];
    }

}