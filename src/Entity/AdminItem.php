<?php

namespace App\Entity;

use App\Repository\AdminItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass=AdminItemRepository::class)
 */
class AdminItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"adminCatalogListItems"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"adminCatalogListItems"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"adminCatalogListItems"})
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="adminItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idCategory;

    /**
     * @ORM\OneToMany(targetEntity=UserItem::class, mappedBy="adminItem")
     */
    private $userItems;

    public function __construct()
    {
        $this->userItems = new ArrayCollection();
    }

    public function updateUserValues()
    {
        foreach ($this->getUserItems() as $item)
            if($item->getAutoupdatable() && $item->getAdminItem() === $this)
                $item->setValuePerOne($this->getValue());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getIdCategory(): ?Category
    {
        return $this->idCategory;
    }

    public function setIdCategory(?Category $idCategory): self
    {
        $this->idCategory = $idCategory;

        return $this;
    }



    /**
     * @return Collection|UserItem[]
     */
    public function getUserItems(): Collection
    {
        return $this->userItems;
    }

    public function addUserItem(UserItem $userItem): self
    {
        if (!$this->userItems->contains($userItem)) {
            $this->userItems[] = $userItem;
            $userItem->setAdminItem($this);
        }

        return $this;
    }

    public function removeUserItem(UserItem $userItem): self
    {
        if ($this->userItems->removeElement($userItem)) {
            // set the owning side to null (unless already changed)
            if ($userItem->getAdminItem() === $this) {
                $userItem->setAdminItem(null);
            }
        }

        return $this;
    }
}
