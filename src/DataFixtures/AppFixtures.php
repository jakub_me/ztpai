<?php

namespace App\DataFixtures;

use App\Entity\AdminItem;
use App\Entity\Category;
use App\Entity\User;
use App\Entity\UserDetails;
use App\Entity\UserItem;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\UserTypes;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager) {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
    }
    public function load(ObjectManager $manager) : void {
        //LOGGING
        //dump($userId = $user3->getId());

        $x = new UserTypes();
        $y = new UserTypes();
        $z = new UserTypes();
        $x->setName("ROLE_ADMIN");
        $y->setName("ROLE_USER");
        $z->setName("ROLE_OFF");
        // $product = new Product();
        $manager->persist($x);
        $manager->flush();
        $manager->persist($y);
        $manager->flush();
        $manager->persist($z);
        $manager->flush();

        $userDet = new UserDetails();
        $userDet->setName("qwe");
        $userDet->setSurname("qwe");
        $userDet2 = new UserDetails();
        $userDet2->setName("qwerty");
        $userDet2->setSurname("qwe2");

        /*$userTypes = new UserTypes();
        $userTypes->setName("ROLE_ADMIN");
*/
        $user = new User();
        $user->setEmail('qwerty1234567890qwertyuiopqwere');
        $user->setPassword($this->encoder->encodePassword($user, 'admin'));
        $user->setUserType($x);
        $user->setUserDetails($userDet);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $user2 = new User();
        $user2->setEmail('a@a.a');
        $user2->setPassword($this->encoder->encodePassword($user2, 'admin'));
        $user2->setUserType($x);
        $user2->setUserDetails($userDet2);

        $this->entityManager->persist($user2);
        $this->entityManager->flush();


        // add new category with first item
        /*$category = $this->getDoctrine()->getRepository(Category::class)->findCategoryByName('monety');
        if(is_null($category))
        {
            $category = new Category();
            $category->setName("monety");
        }*/

        $cat = new Category();
        $cat->setName("karty graficzne");

        /*$category = $this->getDoctrine()->getRepository(AdminItem::class)->findAdminItem(--nazwa, --catid);
        if(!is_null($category))
        {
            return error
        }*/
        $manager->persist($cat);
        $manager->flush();

        $aitem1 = new AdminItem();
        $aitem1->setName("GTX 660");
        $aitem1->setValue(20000);
        $aitem1->setIdCategory($cat);
        $manager->persist($aitem1);
        $manager->flush();

        $aitem2 = new AdminItem();
        $aitem2->setName("GTX 650");
        $aitem2->setValue(10000);
        $aitem2->setIdCategory($cat);
        $manager->persist($aitem2);
        $manager->flush();

        $aitem3 = new AdminItem();
        $aitem3->setName("GTX 760");
        $aitem3->setValue(40000);
        $aitem3->setIdCategory($cat);
        $manager->persist($aitem3);
        $manager->flush();

        $cat2 = new Category();
        $cat2->setName("monety");
        $manager->persist($cat2);
        $manager->flush();

        $aitem4 = new AdminItem();
        $aitem4->setName("zlotowka");
        $aitem4->setValue(100);
        $aitem4->setIdCategory($cat2);
        $manager->persist($aitem4);
        $manager->flush();


        $userDet3 = new UserDetails();
        $userDet3->setName("Jerzy");
        $userDet3->setSurname("Brzęczyszczykiewicz-Brzęczyszczykiewicz");
        $user3 = new User();
        $user3->setEmail('u@u.u');
        $user3->setPassword($this->encoder->encodePassword($user, 'user'));
        $user3->setUserType($y);
        $user3->setUserDetails($userDet3);
        $manager->persist($user3);
        $manager->flush();
        /*$category = $this->getDoctrine()->getRepository(Category::class)->findCategoryByName('monety');
        if(is_null($category))
        {
            $category = new Category();
            $category->setName("monety");
        }*/
        $catuser3 = new Category();
        $catuser3->setName("rozne");
        $manager->persist($catuser3);
        $manager->flush();
        $user3->addCategory($catuser3);
        $user3->addCategory($cat);
        $manager->persist($user3);
        $manager->flush();


        $i1 = new UserItem();
        $i1->setIdUser($user3);
        $i1->setIdCategory($catuser3);
        $i1->setName("dukat");
        $i1->setValuePerOne(7000);
        $i1->setAmount(2);
        $manager->persist($i1);
        //check if not repeating
        $i2 = new UserItem();
        $i2->setIdUser($user3);
        $i2->setIdCategory($catuser3);
        $i2->setName("denar");
        $i2->setValuePerOne(17000);
        $i2->setAmount(1);
        $manager->persist($i2);

        $i3 = new UserItem();
        $i3->setIdUser($user3);
        $i3->setIdCategory($catuser3);
        $i3->setName("gtx 660");
        $i3->setValuePerOne(27000);
        $i3->setAmount(1);
        //wklejanie z cache uzytkownika
        $i3->setAdminItem($aitem1);
        //from checkbox
        $i3->setAutoupdatable(true);
        $manager->persist($i3);
        $manager->flush();

        $catuser4 = new Category();
        $catuser4->setName("rozne2");
        $manager->persist($catuser4);
        $manager->flush();
        $user3->addCategory($catuser4);
        $manager->persist($user3);
        $manager->flush();
        $i4 = new UserItem();
        $i4->setIdUser($user3);
        $i4->setIdCategory($catuser4);
        $i4->setName("gtx 660");
        $i4->setValuePerOne(27000);
        $i4->setAmount(1);
        //wklejanie z cache uzytkownika
        $i4->setAdminItem($aitem2);
        //from checkbox
        $i4->setAutoupdatable(true);
        $manager->persist($i4);

        $manager->flush();




        $userDet4 = new UserDetails();
        $userDet4->setName("Jan");
        $userDet4->setSurname("Nowak");
        $user4 = new User();
        $user4->setEmail('user@u.u');
        $user4->setPassword($this->encoder->encodePassword($user, 'user'));
        $user4->setUserType($y);
        $user4->setUserDetails($userDet4);
        $manager->persist($user4);
        $manager->flush();
        /*$category = $this->getDoctrine()->getRepository(Category::class)->findCategoryByName('monety');
        if(is_null($category))
        {
            $category = new Category();
            $category->setName("monety");
        }*/
        $user4->addCategory($catuser4);
        $user4->addCategory($cat);
        $manager->persist($user4);
        $manager->flush();


        $i11 = new UserItem();
        $i11->setIdUser($user4);
        $i11->setIdCategory($catuser4);
        $i11->setName("dukat");
        $i11->setValuePerOne(7000);
        $i11->setAmount(2);
        $manager->persist($i11);
        //check if not repeating
        $i22 = new UserItem();
        $i22->setIdUser($user4);
        $i22->setIdCategory($cat);
        $i22->setName("denar");
        $i22->setValuePerOne(17000);
        $i22->setAdminItem($aitem3);
        $i22->setAutoupdatable(true);
        $i22->setAmount(1);
        $manager->persist($i22);

        $i33 = new UserItem();
        $i33->setIdUser($user4);
        $i33->setIdCategory($catuser4);
        $i33->setName("gtx 660");
        $i33->setValuePerOne(27000);
        $i33->setAmount(1);
        //wklejanie z cache uzytkownika
        $i33->setAdminItem($aitem3);
        //from checkbox
        $i33->setAutoupdatable(true);
        $manager->persist($i33);
        $manager->flush();


    }
}
